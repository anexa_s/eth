from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='ethereum_tutorials',
    version='0.0.1',
    description='Distributed Applications',
    long_description=readme(),
    long_description_content_type='text/markdown',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent'
    ],
    author='ktk54x',
    author_email='kartikeya.0005@gmail.com',
    keywords='Ethereum',
    packages=['simple_storage'],
    install_requires=[
        'py-solc-x==1.1.1',
        'web3==5.24.0'
    ],
    include_package_data=True,
    entry_points={
            'console_scripts': [
                'simple_storage = simple_storage.__main__:main'
            ]
    }
)
